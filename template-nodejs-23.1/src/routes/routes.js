const { Router } = require('express');
const UserController = require('../controllers/UserController');
const ProductController = require('../controllers/ProductController');
const AddressController = require('../controllers/AddressController');
const router = Router();

//User

router.get('/users',UserController.index);
router.get('/user/:id',UserController.show);
router.post('/user',UserController.create);
router.put('/user/:id',UserController.update);
router.delete('/user/:id',UserController.destroy);

//Product

router.get('/products',ProductController.index);
router.get('/product/:id',ProductController.show);
router.post('/product',ProductController.create);
router.put('/product/:id',ProductController.update);
router.delete('/product/:id',ProductController.destroy);

//Address

router.get('/addresses',AddressController.index);
router.get('/address/:id',AddressController.show);
router.post('/address',AddressController.create);
router.put('/address/:id',AddressController.update);
router.delete('/address/:id',AddressController.destroy);

//relacionamentos

router.post('/product/addUser/user/:userId/product/:productId',ProductController.addUser);
router.post('/product/:id',ProductController.removeUser);
router.post('/address/addUser/user/:userId/address/:addressId',AddressController.addUser);
router.post('/address/:id',AddressController.removeUSer);

module.exports = router;