const { response } = require('express');
const User = require('../models/UserModel');
const Product = require('../models/ProductModel');

//CRUD

const create = async(req,res)=>{
    try{
        const product = await Product.create(req.body);
        return res.status(201).json({message:"Anúncio criado.", product:product});
    }
    catch(err){
        return res.status(500).json({error: err});
    }
};

const index = async(req,res)=>{
    try{
        const products = await Product.findAll();
        return res.status(200).json({products});
    }
    catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res)=>{
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        return res.status(200).json({product});
    }
    catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req,res)=>{
    const {id} = req.params;
    try{
        const [updated] = await Product.update(req.body, {where:{id: id}});
        if (updated){
            const product = await Product.findByPk(id);
            return res.status(200).send(product);
        }
        throw new Error();
    }
    catch(err){
        return res.status(500).json("Produto não encontrado.");
    }
};

const destroy = async(req,res)=>{
    const {id} = req.params;
    try{
        const deleted = await Product.destroy({where:{id: id}});
        if(deleted){
            return res.status(200).json("Produto deletado com sucesso.");
        }
        throw new Error();
    }
    catch(err){
        return res.status(500).json("Produto não encontrado.");
    }
};

const addUser = async(req,res) =>{
    const {userId, productId} =  req.params;
    try{
        const user = await User.findByPk(userId);
        const product = await Product.findByPk(productId);
        await product.setUser(user);
        return res.status(200).json(product);
    }
    catch(err){
        return res.status(500).json({err});
    }
};

const removeUser = async(req,res)=>{
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        await  product.setUser(null);
        return res.status(200).json(product);
    }
    catch(err){
        return res.status(500).json({err});
    }
};

module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    addUser,
    removeUser
};