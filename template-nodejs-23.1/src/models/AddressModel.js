const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Address = sequelize.define('Address',{
    street:{
        type: DataTypes.STRING,
         allownull: false
    },
    number:{
        type: DataTypes.NUMBER,
        allownull: false
    },
    CEP:{
        type: DataTypes.NUMBER,
        allownull: false
    },
    city:{
        type: DataTypes.STRING,
        allownull: false
    },
    neighborhood:{
        type: DataTypes.STRING,
        allownull: false
    }
});

Address.associate = function(models){
    Address.belongsTo(models.User);
};


module.exports = Address;