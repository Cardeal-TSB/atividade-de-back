const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");


const User = sequelize.define('User',{

    name:{
        type: DataTypes.STRING,
        allownull: false
    },
    email:{
        type: DataTypes.STRING,
        allownull: false
    },
    password:{
        type: DataTypes.STRING,
        allownull: false
    },
    birthDate:{
        type: DataTypes.DATEONLY,
        allownull: false
    },
    cellphone:{
        type: DataTypes.NUMBER,
        allownull: true
    },
    CPF:{
        type: DataTypes.NUMBER,
        allownull: false
    }
});

User.associate = function(models){
    User.hasMany(models.Product);
    User.hasMany(models.Address);
};


module.exports = User;