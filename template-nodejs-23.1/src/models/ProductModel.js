const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");


const Product = sequelize.define('Product',{

    name:{
        type: DataTypes.STRING,
        allownull: false
    },
    description:{
        type: DataTypes.STRING,
        allownull: true
    },
    amount:{
        type: DataTypes.NUMBER,
        allownull: true
    },
    price:{
        type: DataTypes.DOUBLE,
        allownull: false
    },
    fabricationDate:{
        type: DataTypes.DATEONLY,
        allownull: true
    },
    delivery:{
        type: DataTypes.BOOLEAN,
        allownull: true
    }
});

Product.associate = function(models){
    Product.belongsTo(models.User);
};


module.exports = Product;